package teramaet.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class CameraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        CameraFragment cameraFragment = new CameraFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.camera,cameraFragment).commit();
    }
}
