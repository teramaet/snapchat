package teramaet.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FriendProfile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_profile);
        FriendProfileFragment friendProfileFragment = new FriendProfileFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.friendprofile,friendProfileFragment).commit();
    }
}
