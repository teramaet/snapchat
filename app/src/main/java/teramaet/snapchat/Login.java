package teramaet.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        LoginFragment loginFragment = new LoginFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.login, loginFragment).commit();
    }

}