package teramaet.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class AddFriend extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AddFriendFragment addFriendFragment = new AddFriendFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container,addFriendFragment).commit();
    }
}
