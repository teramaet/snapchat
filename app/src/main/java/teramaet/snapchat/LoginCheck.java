package teramaet.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LoginCheck extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_check);
        LoginCheckFragment loginCheckFragment = new LoginCheckFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.logincheck,loginCheckFragment);
    }
}
