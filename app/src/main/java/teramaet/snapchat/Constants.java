package teramaet.snapchat;

/**
 * Created by Student on 5/24/2016.
 */
public class Constants {
    public static final String ACTION_ADD_FRIEND = "com.example.teramaet.Snapchat.ADD_FRIEND";
    public static final String ACTION_SEND_FRIEND_REQUEST = "com.example.teramaet.Snapchat.SEND_FRIEND_REQUEST";

    public static final String BROADCAST_ADD_FRIEND_SUCCESS = "com.example.teramaet.Snapchat.ADD_FRIEND_SUCCESS";
    public static final String BROADCAST_ADD_FRIEND_FAILURE = "com.example.teramaet.Snapchat.ADD_FRIEND_FAILURE";

    public static final String BROADCAST_FRIEND_REQUEST_SUCCESS = "com.example.teramaet.Snapchat.FRIEND_REQUEST_SUCCESS";
    public static final String BROADCAST_FRIEND_REQUEST_FAILURE = "com.example.teramaet.Snapchat.FRIEND_REQUEST_FAILURE";
}
