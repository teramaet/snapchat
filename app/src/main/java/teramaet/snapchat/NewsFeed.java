package teramaet.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class NewsFeed extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_feed);
        NewsFeedFragment newsFeedFragment = new NewsFeedFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.news,newsFeedFragment);
    }
}
